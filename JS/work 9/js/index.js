window.onload = () => {
  const query = (a) => document.querySelector(a),
    container = query("div"),
    mSeconds = query('div>div:first-child>span:last-child'),
    seconds = query('div>div:first-child>span:nth-child(2)'),
    minutes = query('div>div:first-child>span:first-child'),
    start = query('div>div:last-child>button:first-child'),
    stop = query('div>div:last-child>button:nth-child(2)'),
    reset = query('div>div:last-child>button:last-child');
  // console.log(container)

  let mCounter = 0, sCounter = 0, Mcounter = 0, intervalHandler;

  const count = () => {
    if (mCounter < 10) {
      (mSeconds.innerText) = '0' + mCounter++;
    };
    if (mCounter >= 10) {
      mSeconds.innerText = mCounter++;
    };
    if (mCounter > 99) {
      mCounter = 0;
      if (sCounter < 10) {
        seconds.innerText = '0' + (1 + sCounter++);
      };
      if (sCounter >= 10) {
        seconds.innerText = sCounter++;
      };
      if (sCounter > 59) {
        sCounter = 0;
        if (Mcounter < 10) {
          minutes.innerText = '0' + (1 + Mcounter++);
        };
        if (Mcounter >= 10) {
          minutes.innerText = Mcounter++;
        };
        if (Mcounter > 59) {
          Mcounter = 0;
        };
      };
    };
  };

  replace = (a) => {
    for (let item of container.classList) {
      if (item !== "container-stopwatch") {
        container.classList.replace(item, a);
      };
    };
  };

  start.onclick = () =>{
    intervalHandler = setInterval(count, 10), replace('green')
    start.disabled = true;
  }

  stop.onclick = () => { clearInterval(intervalHandler), replace('red'), start.disabled = false;}

  reset.onclick = () => {
    rec = Mcounter + 'min' + " " + sCounter + 'sec' + " " + mCounter + 'ms' + " ";
    console.log(rec);
    mCounter = 0;
    sCounter = 0;
    Mcounter = 0;
    mSeconds.innerText = '0' + mCounter;
    seconds.innerText = '0' + sCounter;
    minutes.innerText = '0' + Mcounter;
    replace('silver');
    
  };
};