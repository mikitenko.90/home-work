window.addEventListener('DOMContentLoaded', () => {
  let data;

  let button = document.querySelector('.btn-info'),
    spinner = document.querySelector(".spinner-border");
  spinner.classList.add('hide')
  button.addEventListener('click', () => {
    spinner.classList.remove('hide')
    let item1 = "https://swapi.dev/api/people/?page=1&format=json",
      item2 = "https://swapi.dev/api/people/?page=2&format=json",
      item3 = "https://swapi.dev/api/people/?page=3&format=json",
      item4 = "https://swapi.dev/api/people/?page=4&format=json",
      item5 = "https://swapi.dev/api/people/?page=5&format=json",
      item6 = "https://swapi.dev/api/people/?page=6&format=json",
      item7 = "https://swapi.dev/api/people/?page=7&format=json",
      item8 = "https://swapi.dev/api/people/?page=8&format=json",
      item9 = "https://swapi.dev/api/people/?page=9&format=json";
    const arr = [item1, item2, item3, item4, item5, item6, item7, item8, item9];
    arr.forEach(i => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', i);
      xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
          data = JSON.parse(xhr.responseText)
          setTimeout(show, 2000, data)
          
        };


      })
      xhr.send()
    })


  })


  function show(res) {
    let cardGroop = document.createElement('div');
    cardGroop.className = "row row-cols-1 row-cols-sm-3 g-1"
    res.results.forEach(element => {
      const card = document.createElement('div'),
        imgUpVrap = document.createElement('div'),
        imgVrap = document.createElement('div')
      img = document.createElement('img')
      cardBodyWrap = document.createElement('div'),
        cardBody = document.createElement('div'),
        title = document.createElement('h5'),
        height = document.createElement('p'),
        mass = document.createElement('p'),
        hair_color = document.createElement('p'),
        skin_color = document.createElement('p');

      card.className = 'card';
      imgUpVrap.className = "row g-0"
      imgVrap.className = "col-5 col-sm-4"
      img.className = "img-fluid w-100"
      img.src = `https://starwars-visualguide.com/assets/img/characters/${element.url.match(/\d{1,}/)[0]}.jpg`
      cardBodyWrap.className = "col-7 col-sm-8"
      cardBody.className = "card-body"
      title.className = 'card-title';
      height.className = 'card-text';
      mass.className = 'card-text';
      hair_color.className = 'card-text';
      skin_color.className = 'card-text';

      spinner.after(cardGroop);
      cardGroop.append(card);
      card.append(imgUpVrap);
      imgUpVrap.append(imgVrap);
      imgVrap.append(img);
      imgVrap.after(cardBodyWrap);
      cardBodyWrap.append(cardBody);
      cardBody.append(title);
      title.after(height);
      height.after(mass);
      mass.after(hair_color);
      hair_color.after(skin_color);

      title.innerHTML = `name: ${element.name}`
      height.innerHTML = `height: ${element.height}`
      mass.innerHTML = `mass: ${element.mass}`
      hair_color.innerHTML = `hair_color: ${element.hair_color}`
      skin_color.innerHTML = `skin_color: ${element.skin_color}`

      spinner.classList.add('hide')
    });
  };


});