window.onload = () => {
let container = document.createElement('div');
let button = document.querySelector('input');
button.after(container)
button.onclick = () => {
  function can(a = prompt('recomendet-UserSize', ("100"))) {
    container.style.width = a * 10 + 'px';
    for (let i = 0; i < 100; i++) {
      let canvas = document.createElement('canvas'),
        context = canvas.getContext('2d');
      canvas.style.width = a + 'px';
      context.beginPath();
      context.arc(150, 75, 73, 0, 2 * Math.PI, false);
      context.fillStyle = `rgb(${Math.floor(Math.random() * 360)}, ${Math.floor(Math.random() * 360)}, ${Math.floor(Math.random() * 360)})`
      context.fill();
      context.lineWidth = 1;
      context.stroke();
      container.append(canvas);
      canvas.onclick = () => canvas.remove();
    };
  };
  can()
};
};