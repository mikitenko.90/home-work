import createTag from "./createTag.js"

window.onload = () => {
    let body = document.body;
    const topHeader = createTag("header", "top-header", null, "тег хедер верх"),
        main = createTag("main"),
        footer = createTag("footer", "footer", null, "footer"),
        sectionLeft = createTag("section", "section-l", null, "section"),
        sectionRigint = createTag("section", "section-r", null, "section"),
        articleTop = createTag("article", "article", null, "article"),
        articleBot = createTag("article", "article", null, "article"),
        articleTopDiv = createTag("div");
    body.className = "body";
    body.after(main)
    body.before(topHeader)
    main.after(footer)




    topHeader.append(createTag("nav", "header-nav", null, "navigation"));
    main.append(sectionLeft, sectionRigint);
    sectionLeft.append(
        createTag("header", "section-header", null, "хедер left"),
        articleTop,
        articleBot,
        createTag("footer", "footer-left", null, "footer"),
    );
    sectionRigint.append(
        createTag("header", "section-header", null, "хедер right"),
        createTag("nav", "main-nav", null, "NAV"),
    );

    articleTop.append(
        createTag("header", "article-header", null, "Article Header"),
        createTag("p", "article-p", null, "P"),
        articleTopDiv,
        createTag("footer", "article-footer", null, "footer"),
    
    );
    articleBot.append(
        createTag("p", "article-p", null, "p"),
        createTag("p", "article-p", null, "p"),
        createTag("footer", "article-footer", null, "footer"),
    );
    articleTopDiv.append(
        createTag("p", "article-p", null, "p"),
        createTag("p", "article-p", null, "aside"),
    );
}





