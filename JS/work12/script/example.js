// First attempt

window.addEventListener('load', () => {
  let img = document.createElement('img'),
    table = document.querySelector('.table')
  img.src = './Pizza_pictures/mocarela_1556623220308.png'
  table.append(img);

  let ingridients = document.querySelector('.ingridients'),
    [...draggable] = document.querySelectorAll('.draggable')
  // console.log(draggable)



  ingridients.addEventListener('mousedown', function (e) {

    
    draggable.forEach((item) => {
      if (e.target == item) {

        let positionNotInGoal = ()=>{
          e.target.style.position = 'relative';
          e.target.style.width = '100%'
          e.target.style.top = '0px'
          e.target.style.left = '0px'

        };

        let position = () => {
          e.target.style.position = 'absolute';
          e.target.style.width = '150px'
          e.target.style.zIndex = 1000; 
        };
        position();

        function moveAt(pageX, pageY) {
          e.target.style.left = pageX - e.target.offsetWidth / 2 + 'px';
          e.target.style.top = pageY - e.target.offsetHeight / 2 + 'px';
        };
        moveAt(e.pageX, e.pageY);

        function onMouseMove(event) {
          moveAt(event.pageX, event.pageY);
        }
        document.addEventListener('mousemove', onMouseMove);

        e.target.onmouseup = function () {
          document.removeEventListener('mousemove', onMouseMove);
          positionNotInGoal()
          e.target.onmouseup = null;

        };
        e.target.ondragstart = function () {
          return false;

        };

      }
    })
  })

  let span = document.querySelectorAll('.ingridients>div>span')
 console.log(span)
})