
window.addEventListener('DOMContentLoaded', () => {
  const pizza = document.querySelector('.pizza-wrep'),
    size = document.querySelector('#pizza'),
    ingridients = document.querySelector('.ingridients'),
    [...draggable] = document.querySelectorAll('.draggable'),
    sous = document.querySelector('.sauces>div>div'),
    topings = document.querySelector('.topings>div>div'),
    price = document.querySelector('.price>div>span');
  let totalSum = (a, b) => a + b,
    priceSize = 350,
    priceAddings = null;
  price.textContent = totalSum(priceSize, priceAddings);



  size.addEventListener('click', (e) => {
    if (e.target.value == 'small') {
      priceSize = 150;
      price.textContent = totalSum(priceSize, priceAddings)

    }
    if (e.target.value == 'mid')
      priceSize = 250;
    price.textContent = totalSum(priceSize, priceAddings)

    if (e.target.value == 'big') {
      priceSize = 350;
      price.textContent = totalSum(priceSize, priceAddings)
    }
  });


  function check(mass, eqel) {
    mass.forEach((el) => {
      if (el.name == eqel.name) {
        current.draggable = false;
      }
      else current.draggable = true;
    })
  }

  function remove(area) {
    area.addEventListener('click', (e) => {
      e.stopPropagation(),
        [...alreadyIn] = document.querySelectorAll('.pizza-wrep>img')
      if (e.target !== area) {
        alreadyIn.forEach((el) => {
          if (el.id == e.target.id - 1) {
            e.target.remove();
            priceAddings += -e.target.price
            price.textContent = totalSum(priceSize, priceAddings);
            el.remove();
            draggable.forEach((elem) => {
              if (e.target.textContent == elem.nextElementSibling.textContent) {
                elem.draggable = true;
              }
            })
          }
        })
      }
    }, true)
  }

  function drugNdrop() {
    pizza.addEventListener('dragover', (e) => e.preventDefault());

    ingridients.addEventListener('dragstart', (e) => {
      draggable.forEach((item) => {
        if (item == e.target) {
          current = e.target;
        };
      })
    });

    pizza.addEventListener('drop', function (e) {
      let clone = current.cloneNode(true)
      let cloneSpan = current.nextElementSibling.cloneNode(true)
      let random = () => Math.floor(Math.random() * 360);
      clone.id = (random())
      cloneSpan.id = ((parseInt(clone.id) + 1))
      this.append(clone);
      let [...alreadyIn] = document.querySelectorAll('.pizza-wrep>img'),
        addings = current.nextElementSibling.textContent;



      if (addings === "Кетчуп" || addings === "BBQ" || addings === "Рiкотта") {
        sous.append(cloneSpan);
        cloneSpan.price = 10;
        priceAddings += cloneSpan.price;
        SpanSous = [...sous.children];
        check(alreadyIn, current);
        remove(sous);


      }

      if (addings === "Сир звичайний" || addings === "Сир фета" || addings === "Телятина" || addings === "Помiдори" || addings === "Моцарелла" || addings === "Гриби") {
        topings.append(cloneSpan);
        cloneSpan.price = 25;
        priceAddings += cloneSpan.price;
        SpanTopings = [...topings.children];
        check(alreadyIn, current);
        remove(topings);

      };
      price.textContent = totalSum(priceSize, priceAddings);
    });
  }

  drugNdrop()


  const validation = () => {
    const forms = document.forms[1],
      [...formsElement] = forms.elements,
      paternText = /[a-zA-Z]+/,
      paternNum = /^[0]\d{9}$/,
      paternEmail = /\b[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\.[a-z]{2,4}\b/;
    inputChecked = false;

    formsElement.forEach((input) => {
      if (input == formsElement[0] || input == formsElement[1] || input == formsElement[2]) {
        input.onchange = validationInputs;
      }

    })


    function validationInputs(e) {
      if (this.type === 'text') {
        const res = this.value.search(paternText);
        if (res === 0) {
          this.className = 'success'
        } else this.className = 'error'
      }
      if (this.type === 'tel') {
        const res = this.value.search(paternNum);
        if (res === 0) {
          this.className = 'success'
        } else this.className = 'error'
      }
      if (this.type === 'email') {
        const res = this.value.search(paternEmail);
        if (res === 0) {
          this.className = 'success'
        } else this.className = 'error'
      }
      inputChecked = true;
    }


    formsElement[3].addEventListener('click', () => {
      forms.reset();
      inputChecked = false;
      formsElement.forEach((input) => {
        if (input.classList == 'success') {
          input.classList.remove('success');
        }
        if (input.classList == 'error') {
          input.classList.remove('error');
        }
      })
    })


    formsElement[4].addEventListener('click', () => forms.requestSubmit())

    forms.addEventListener('submit', function (e) {

      let invalid = false;
      formsElement.forEach((input) => {
        if (input.type == 'text' || input.type == 'tel' || input.type == 'email') {
          if (inputChecked) {
            input.onchange()
            if (input.className == 'error') {
              invalid = true;
            }
          }
        }
      })
      if (invalid) {
        alert('You have mistakes');
        e.preventDefault()
      }
      if (!inputChecked) {
        alert('I need your data')
        e.preventDefault()
      }

    })
  };
  validation()
});






