// My class_______________
class Human{
static icon = "⚓";
constructor (a, b, c) {
  this.name = a;
  this.age = b;
  this.sex = c;
};
};

// my array_________
let arr = [new Human("Ivan", 28, "male"), new Human("Inna", 16, "female"), new Human("Alex", 14, "male"), new Human("Alex", 14, "male"), new Human("Piper", 44, "female")];

// sorting (arr) by Human.age (up and down)________________________
let sorting = (a, b = prompt("+ ; -")) => {
  b === "+" ? a.sort((c, d) => c.age - d.age) : a.sort((c, d) => d.age - c.age)
  return a
};

// input information in browser  1 -> arr then 2 -> sorting(arr)__________________
let Info = a => a.map(item => document.body.innerHTML += (Object.values(item).join(' ') + ("<br>")));

// aditional function, just for easy call all together.
let call = () => {
  Info(arr);
  document.body.innerHTML += ("<hr>" + "<br>");
  Info(sorting(arr));
  document.body.innerHTML += ("<hr>" + "<br>");
  console.log(arr[1])
  console.log(Human.icon)
};
call()
