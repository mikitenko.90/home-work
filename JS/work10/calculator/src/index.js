window.addEventListener("DOMContentLoaded", () => {
    const btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input"),
        point = document.querySelector('#point'),
        zero = document.querySelector('#zero'),
        buttonEql = document.querySelector('.orange'),

        patternPoint = /[.]/,
        patternValue = /[\d.]/,
        patternZero = /0/,
        patternTwoNumber = /\d\d/;


    btn.addEventListener("click", function (e) {
        // first number
        let key = e.target.value;

        if (patternValue.test(key) && calc.oper == '' && calc.eql == '') {
            calc.value1 += key
            show(calc.value1, display);
             // case - when 0 first poind add themself.
            if (  calc.value1 === '0') {
                point.click()
            };
            //case - when two points unacceptable in  calc.value1 
            patternPoint.test(calc.value1) ? point.disabled = true : point.disabled = false
            // case - when zero allow exist after point in  calc.value1 
            if (patternPoint.test(calc.value1)) {
                zero.disabled = false;
            };
            // case - when zero allow exist after  number before point in  calc.value1 
            if (patternTwoNumber.test(calc.value1)) {
                zero.disabled = false;
            };

        };

        // Second number
        if (patternValue.test(key) && calc.value1 !== '' && calc.oper !== '') {
            calc.value2 += key
            console.log(typeof calc.value2)
            show(calc.value2, display);
            // case - when I unlock button '='
            if (calc.value2 !== '') {
                buttonEql.disabled = false
            };
            //case - when two points unacceptable in  calc.value2 
            patternPoint.test(calc.value2) ? point.disabled = true : point.disabled = false
            // case - when zero allow exist after point in  calc.value2
            if (patternPoint.test(calc.value2)) {
                zero.disabled = false;
            };
            // case - when zero allow exist after  number before point in  calc.value2 
            if (patternTwoNumber.test(calc.value2)) {
                zero.disabled = false;
            };
            // case - when 0 first poind add themself.
            if (calc.value2 === '0') {
                point.click()
            };
        };

        // operation
        if (key === "+" || key === "-" || key === "*" || key === "/") {
            calc.oper = key
            show(calc.oper, display)
        };

        // equals
        if (key === '=') {
            calc.eql = key
            if (calc.oper === '+') {
                add(calc.value1, calc.value2)
            };
            if (calc.oper === '*') {
                mul(calc.value1, calc.value2)
            };
            if (calc.oper === '/') {
                div(calc.value1, calc.value2)
            };
            if (calc.oper === '-') {
                sub(calc.value1, calc.value2)
            };

            show(calc.rec, display)
            calc.value1 = '';
            calc.value2 = '';
            calc.oper = '';
            calc.eql = '';
            buttonEql.disabled = true;

        };

        // delete result
        if (key === 'C') {
            calc.value1 = '';
            calc.value2 = '';
            calc.oper = '';
            calc.eql = '';
            display.value = '0';
            calc.rec = '';
        };

        // memory save
        if (key === 'm+') {
            calc.memory = display.value;

        }

        // memory delete
        if (key === 'm-') {
            calc.memory = '';
        };

        // memory show
        if (key === 'mrc') {
            calc.value1 = calc.memory;
            calc.value2 = '';
            calc.oper = '';
            calc.eql = '';
            show(calc.memory, display)
        };

    }), false
});


const calc = {
    value1: '',
    value2: '',
    oper: '',
    eql: '',
    rec: '',
    memory: '',


};

function show(value, el) {
    el.value = value
};


add = (a, b) => { return calc.rec = parseFloat(a) + parseFloat(b) };
mul = (a, b) => { return calc.rec = parseFloat(a) * parseFloat(b) };
sub = (a, b) => { return calc.rec = parseFloat(a) - parseFloat(b) };
div = (a, b) => {
    b !== '0' ? calc.rec = parseFloat(a) / parseFloat(b) : calc.rec = 'error'
    return calc.rec
};


