/*
 ДЗ 01.10
1.Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

2.Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

3.Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці


4.Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

5.Створіть поле для введення даних у полі введення даних виведіть текст під полем
*/


window.addEventListener('DOMContentLoaded', () => {
  // task # 1
  let listRed = document.querySelector('ul>li:nth-child(3)'),
    listBlue = document.querySelector('ul>li:nth-child(1)');
  listBlue.classList.add('blue');
  listRed.classList.add('red');

  // task # 2
  let div = document.querySelector('div');
  div.classList.add('task-2');
  div.addEventListener('mousemove', function (e) {
    this.innerHTML = `X: ${e.offsetX} Y: ${e.offsetY}`
  })

  // task # 3 
  let move = document.createElement('div');
  div.after(move);
  move.classList.add('move');
  move.addEventListener('mouseover', function (e) {
    this.classList.add('move');
    this.style.position = 'absolute'
    this.style.top = `${e.offsetY + 25}px `
    this.style.left = `${e.offsetX + 25}px `
  });

  // task # 4 and #5
text = '';
document.querySelector('input').addEventListener('keypress', function(e) {
text += e.key;
console.log(text);
document.querySelector('output').innerText += e.key;
  })
});






